// console.log('hello from JS file');

// lets target foirst our form component and place it inside a new variable
let registerForm = document.querySelector("#registerUser")

// inside the first param of the method to describe the event that it will listen to, while insied the 2nd parameter lets decribe the action/ procedure that will happen upon triggering the said event.
registerForm.addEventListener("submit",(event) =>{
	event.preventDefault() //to aviod page refresh/page redirection once that the said event has been triggered.


	//capture each values inside the input fileds first, then repackage them inside a new variable
	let firstName = document.querySelector("#firstName").value
	//lets create a checker to make sure that we are succesful in capturing the values.
	console.log(firstName)
	let lastName = document.querySelector("#lastName").value
	//lets create a checker to make sure that we are succesful in capturing the values.
	console.log(lastName)
	let email = document.querySelector("#userEmail").value
	//lets create a checker to make sure that we are succesful in capturing the values.
	console.log(email)
	let mobileNo = document.querySelector("#mobileNumber").value
	//lets create a checker to make sure that we are succesful in capturing the values.
	console.log(mobileNo)
	let password = document.querySelector("#password1").value
	//lets create a checker to make sure that we are succesful in capturing the values.
	let verifyPassword = document.querySelector("#password2").value
	//lets create a checker to make sure that we are succesful in capturing the values.
	// console.log(verifyPassword)
	//lets create a data validation for our register page. WHY?
	//why do we need to validate data? .. to check and verify if the data that wwe will accept is accurate.
	//we do data validation to make sure that the storage space will be properly utilizes

	//the following info/data that we can validate 
	//email, password. mobileNo.
	//lets create a control structure to determine the respond the next set of procedure before the user can register a new account.
	//it will be a lot efficient for you as a developer to sanitize the data before submitting it to the backend /server for processing
   if((firstName  !== "" && lastName !== ""  && email !== ""  && password !== ""  && verifyPassword !== "" ) && (password === verifyPassword) && (mobileNo.length === 11)){  
      //how are we going to integrate our email-exists method? 
      //we are going to send out a new request 
      //before you allow the user to create a new account check if the email value is still available for use. this will ensure that each user will have their unique user email 
      //upon sending this request we are instantiating a promise object, which means we should apply a method to handle the response. 
      fetch('https://serene-eyrie-45105.herokuapp.com/api/user/email-exists', {
         method: 'POST',
         headers: {
             'Content-Type': 'application/json'
         }, 
         body: JSON.stringify({
            email: email
         })
      }).then(res => {
          return res.json() //to make it readable once the response returns to the client side. 
      }).then(convertedData => { 
          //what would the response look like? 
          //lets create a control structure to determine the proper procedure depending on the response 
         if (convertedData === false) {
              //lets allow the user to register an account. 
               fetch('https://serene-eyrie-45105.herokuapp.com/api/user/register', {
                //we will now describe the structure of our request for register
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                //API only accepts request in a string format. 
                body: JSON.stringify({
                  firstName: firstName,
                  lastName: lastName,
                  email: email,
                  mobileNo: mobileNo, 
                  password: password
                }) 
             }).then(res => {
                console.log(res)
                //console.log("hello");
                return res.json()
             }).then(data => {
                console.log(data)
                 //lets create here a control structure to give out a proper response depending on the return from the backend. 
                if(data === true){
                   Swal.fire("New Account Registered Successfully!")
                }else{
                   //inform the user that something went wrong
                    Swal.fire("Something is wrong!")
                }
              }
              )
           } else {
               //lets inform the user what went wrong 
               Swal.fire("The Email Already exist!") // you can modify the message. 
          }
      }
      )

   	  //this block of code will run if the condition has been met
      //how can we create a new account for user using the data that he/she entered?
      //url -> describes the destination of the request. 
      //3 STATES for a promise (pending, fullfillment, rejected)
   }else{
       Swal.fire("Oh no!!!")
   }
}) 