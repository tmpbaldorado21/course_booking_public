//get the value of the access token inside the local storage and place it inside a new variable. 
let token = localStorage.getItem("token"); 
console.log(token)

let container = document.querySelector("#profileContainer")
//our goal here is to display the information about the user details 
//send a request to the backend project
fetch('https://serene-eyrie-45105.herokuapp.com/api/user/details', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
}).then(res => res.json())
.then(jsonData => {
  if (jsonData){
      let userSubjectsPromises = jsonData.enrollments.map(subject => {
       return fetch(`https://serene-eyrie-45105.herokuapp.com/api/course/${subject.courseId}`).then(res => res.json()).then(SubjectData =>{
        courseName = SubjectData.name
        console.log(courseName)
      return (
// create a container that will serve as the html boilerplate that wil display the properties of the subject that want to get.
`
<tr>
<td> ${SubjectData.name} </td>
<td> ${SubjectData.description} </td>
<td> ${subject.enrolledOn} </td>
<td> ${subject.status} </td>  
</tr>

`

)
})      
});
      return Promise.all(userSubjectsPromises).then(results =>{
        const newHtml = results.join("");
        document.getElementById("profileContainer").innerHTML = `
<div class="col pt-5">
<section class="jumbotron" style="background-color: #e9e6e1">
<ul>
<li class="studInfo">
<h3 class="">First Name : ${jsonData.firstName}</h3>
<h3 class="">Last Name : ${jsonData.lastName}</h3>
</li>
<li class="studInfo">
<h3 class="">Email : ${jsonData.email}</h3>
<h3 class="">Mobile Number : ${jsonData.mobileNo}</h3>
</li>
</ul>

<table class="table">
<tr>
<th>Course Name: </th>
<th>Enrolled On: </th>
<th>Status: </th>
<tbody> ${newHtml}</tbody>
</tr>
</table>
</section>
</div>
`;
});
} 
}).catch(err => {
  console.log(err);
});