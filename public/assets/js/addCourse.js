//create our add course page
let formSubmit = document.querySelector('#createCourse')
//lets acquire an even that will applied in our form component.
//create a subfunction inside the method describe the procedure/action that will take place upon triggering the event.
formSubmit.addEventListener("submit",(event) => {
	event.preventDefault()//this will avoid page redirection
	//lets target the values of each component inside the forms.
	let name = document.querySelector("#courseName").value			
	let cost = document.querySelector("#coursePrice").value
	let desc = document.querySelector("#courseDesc").value
	//lets create a checker to see if we were able to capture the values of each input fields.
	// console.log(Name)
	// console.log(cost)
	// console.log(desc)
	//upon creating this fetch api request we are instantiating a promise
   if(name  !== "" && cost !== ""  && desc !== ""){  
      //how are we going to integrate our email-exists method? 
      //we are going to send out a new request 
      //before you allow the user to create a new account check if the email value is still available for use. this will ensure that each user will have their unique user email 
      //upon sending this request we are instantiating a promise object, which means we should apply a method to handle the response. 
      fetch('https://serene-eyrie-45105.herokuapp.com/api/course/exists', {
         method: 'POST',
         headers: {
             'Content-Type': 'application/json'
         }, 
         body: JSON.stringify({
            name: name
         })
      }).then(res => {
          return res.json()
      }).then(convertedData => { 
         if (convertedData === false) {
               fetch('https://serene-eyrie-45105.herokuapp.com/course/create', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  name: name,
                  description: desc,
                  price: cost
                }) 
             }).then(res => {
                console.log(res)
                return res.json()
             }).then(data => {
                console.log(data)
                if(data === true){
                   alert("New Course has been added!")
                }else{
                    alert("Incorrect input of Data!")
                }
              }
              )
           }
      }
      )
   }else{
        alert("Oh no!!!")
   }
}) 