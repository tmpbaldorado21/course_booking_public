//LETS targer our from component inside our document.
let loginForm = document.querySelector('#loginUser')

//the login form will be used by the client to insert his/her account to be authenticated by the app.
loginForm.addEventListener("submit", (event) => {
	event.preventDefault()

	//lets vapture the values our form components.
	let email =	document.querySelector("#userEmail").value
	let pass =	document.querySelector("#password").value

	//lets create a checker to confirm the acquired values.
	// console.log(email)
	// console.log(pass)

	//our next task is to validate the data inside the input fields first.
   if (email == "" || pass == "" )
    {
    	Swal.fire("Please fill up your Login Form!")
    } 
   else
    {
    	//send a request to the desired enpoint.
    	fetch("https://serene-eyrie-45105.herokuapp.com/api/user/login", {
    		method: 'POST',
    		headers: {
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			email: email, 
    			password: pass
    		})
    	}).then(res => {
    		return res.json()
    	}).then(dataConverted => {
           // console.log(dataConverted.accessToken)
           if (dataConverted.accessToken){
               localStorage.setItem('token', dataConverted.accessToken)
                // Swal.fire("successfully generated access token"); we just created this as confirmation of the previous task.
                fetch('https://serene-eyrie-45105.herokuapp.com/api/user/details', {
                    headers: {
                        //lets pass on the value of our accses token.
                        'Authorization': `Bearer ${dataConverted.accessToken}`
                        //upon sending this request we are instantiating a promise that can lead to 2 posiible outcomes.. what do we need to do to hande the possible outcom states of this promise?

                    }
                }).then(res =>{
                    return res.json()
                }).then(data => {
                    console.log(data)
                    //fetching the user details/ infor is a success
                    //save the "id", "isAdmin"
                localStorage.setItem("id", data._id)
                localStorage.setItem("isAdmin", data.isAdmin)
                    //as developers its up to you if you want to create a checker to make sure that all values are saved properly inside the web storage.
                    // console.log("items are set in Local Storage")
                    window.location.replace('./profile.html')
                }).then(admin => {
                    localStorage.getItem(true)
                    window.location.replace('./course.html')
                })
           }else {
                 Swal.fire("Something is wrong in your Email or Password!")
           }
           
    	})
    }  
}) 