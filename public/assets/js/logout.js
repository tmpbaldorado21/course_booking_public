/*console.log('Hello Logout');*/

//upon logging out the credentials of the user should be remove from the local storage//

//how to delete information inside local storage
localStorage.clear();

//the clear method will wipe out/remove the contents of the local storage

//upon clearing out the local storage property redirect the user back to the login page.

window.location.replace("login.html")

