let urlValues = new URLSearchParams(window.location.search)
console.log(urlValues);

let id = urlValues.get("courseId")
console.log(id);

let token = localStorage.getItem('token')
// console.log(token);

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let desc = document.querySelector("#courseDesc")
let enroll = document.querySelector("#enrollmentContainer")

function findCourseId(array, key, value){
          for(var i =0; i  < array.length; i++){
               if(array[i][key] === value){
                    return array[i];
               }
          }
          return null
     }
fetch(`https://serene-eyrie-45105.herokuapp.com/api/course/${id}`).then(res => res.json()).then(convertedData => {
	// console.log(convertedData)
		name.innerHTML = `<h5> Course Name: </h5> ${ convertedData.name}`
		price.innerHTML = `<h5> Price Name: </h5> ${convertedData.price}`
		desc.innerHTML = `<h5> Description: </h5> ${convertedData.description}`
     fetch('https://serene-eyrie-45105.herokuapp.com/api/user/details',{
          method: 'GET',
          headers: {
               'Content-Type': 'application/json',
               'Authorization': `Bearer ${token}`
          }
          }).then(res=>res.json()).then(userData=>{
               userEnrolled = userData.enrollments
               console.log(userEnrolled)

               let enrollmentDetails = findCourseId(userEnrolled, 'courseId', id);
               console.log(enrollmentDetails);

               if(enrollmentDetails != null){
                    enroll.innerHTML = `<div id="course">
                    <p><h5>Status:</h5> ${enrollmentDetails.status}</p>
                    <p><h5>Enrolled:</h5> ${enrollmentDetails.enrolledOn}</p>`
               }
               else{
                    enroll.innerHTML = `<a href="" id="enrollButton" class="btn btn-dark text-white btn-block">Enroll</a>`
                    enrollBtn();
               }
     });
		
})

     function enrollBtn(){

	document.querySelector("#enrollButton").addEventListener("click", () => {
               fetch('https://serene-eyrie-45105.herokuapp.com/api/user/enroll', {
               		method: 'POST',
               		headers: {
               			'Content-Type': 'application/json',
               			'Authorization': `Bearer ${token}`
               		},
               		body: JSON.stringify({
               			courseId: id
               		})
               })
               .then(res => {
               		return res.json()
               }).then(convertedResponse => {
               		console.log(convertedResponse)
               		if (convertedResponse === true) {
               			alert('Thank you for Enrolling')
               			window.location.replace("./course.html")
               		} else {
               			alert('Sorry something is missing!')
               		}
               })
	})	
}